import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div className='home-main-div'>
      <div className='img-div'>
        <img className='img' src='https://media.licdn.com/dms/image/D5603AQF0NHJ9BQxBYg/profile-displayphoto-shrink_800_800/0/1718270086080?e=1724889600&v=beta&t=P6pUFPfzWP2oy7MHDq7a8wDmo-6aYm1bNI7jkJi3yzo' width='300' height='300'></img>
      </div>
      <div className='div-2'>
        <h6 className='   ml-10  text-3xl font-medium' id='head-font'>HI I AM VHARUN S | MERN Stack Developer</h6>
        <p className='mt-5 ml-10 text-2xl' id='home-para'>
        Welcome to my portfolio!<br></br> I am Vharun, a passionate self-taught MERN Stack Developer <br></br>with a proven track record in building dynamic web applications.<br></br> Skilled in HTML, CSS, JavaScript, React, MongoDB, Express, Node.js<br></br> Git, and GitHub. <br></br>Eager to contribute to innovative projects and teams.
        </p>
        <div className='btn-div'>
         <Link to='https://wa.me/+916374191796?text=Hi Vharun!!' target='_blank' ><button className='btn' >Connect with me</button></Link> 
         </div>
      </div>
      
    </div>
  )
}

export default Home