import React from 'react'

const About = () => {
  return (
    <div className='con-head'>
      <div className='about'>
      <div>
        <h1 className='ABOUT'>About Me</h1>
        <p className='PARA'>I embarked on my journey to become a MERN stack developer<br></br> through self-learning, leveraging online resources and tutorials. My <br></br>dedication and curiosity have driven me to master the intricacies of <br></br>full-stack development, enabling me to build and deploy robust <br></br>web applications.</p>
        <p className='PARA'>Self-taught MERN Stack Developer with a passion for building<br></br> dynamic, responsive web applications using MongoDB, Express.js,<br></br> React, and Node.js.</p>
      </div>
      
      <div className='d'>
          <h1 style={{marginTop:'10px',fontFamily:'sans-serif',fontSize:'1.5rem',textAlign:'center'}}>My Skills</h1> 
        <div className="container"> 
            <p style={{fontSize:'1rem'}}>HTML:90%</p> <div className="skill-html"></div>
        </div> 
        <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>Css : 80%</p>
            <div className="skill-php"></div> 
        </div> 
        <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>JS : 80%</p>
            <div className="skill-php"></div> 
        </div> 
        <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>ReactJs : 80%</p>
            <div className="skill-php"></div>  
       </div>
       <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>NodeJs : 70%</p>
            <div className="skill-php"></div>  
       </div>
       <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>ExpressJs : 70%</p>
            <div className="skill-php"></div>  
       </div>
       <div className="container-2"> 
            <p style={{fontSize:'1rem'}}>MongoDB : 70%</p>
            <div className="skill-php"></div>  
       </div>     

        </div>
        </div>
   
      <div className='main-bio'>
        <div className='biodata'>
          <div className='general'><p className='key'>Birthday:</p><p className='value'>5 June 2005</p></div>
          <div className='general'><p className='key'>Website:</p><p className='value'>WWW.Domain.com</p></div>
          <div className='general'><p className='key'>Degree:</p><p className='value'>B.E(CSE)</p></div>
          <div className='general'><p className='key'>City:</p><p className='value'>Cuddalore</p></div>
        </div> 
        <div className='biodata-2'>
          <div className='general'><p className='key'>Age:</p><p className='value'>20</p></div>
          <div className='general'><p className='key'>Email:</p><p className='value'><a href='mailto:vharunsivaraj06@gmail.com' method='post' enctype='text/plain' className='a'>vharunsivaraj06@gmail.com</a></p></div>
          <div className='general'><p className='key'>Phone:</p><p className='value'>+91-6374191796</p></div>
          <div className='general'><p className='key'>Freelance:</p><p className='value'>Available</p></div>
        </div>
      </div>
  </div>
  )
}

export default About