import { Route, Routes } from "react-router-dom";
import Nva from "./Nva";

import Contact from "./Contact";
import Home from "./Home";
import About from "./About";
import Projects from "./Projects";
import Skill from "./Skill";


function App() {
 
  return (
    <div>
      <Nva/>
     <Routes>
       <Route path="/" element={<Home/>} ></Route>
       <Route path="/contact" element={<Contact/>} ></Route>
       <Route path="/about" element={<About/>} ></Route>
       <Route path="/skills" element={<Skill/>}></Route>
      </Routes>   

    </div>
  )
}

export default App;
