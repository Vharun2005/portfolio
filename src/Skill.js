import React from 'react'

const Skill = () => {
  return (
    <div>
        <h1 style={{textAlign:'center',marginTop:'100px',fontSize:'1.5rem'}}>Technical-Skills</h1>
     <div className='front-end'>
         <div className='card'>
            <img className='html'  src='https://www.freeiconspng.com/thumbs/html5-icon/w3c-html5-logo-0.png' alt=''></img>
            <p>I learned HTML through YouTube and free<br></br> resources, developing strong web<br></br> development skills to contribute to<br></br> innovative projects.</p>
         </div>
         <div className='card'>
            <img className='css' src='https://raw.githubusercontent.com/github/explore/6c6508f34230f0ac0d49e847a326429eefbfc030/topics/css/css.png' alt=''></img>
            <p>I learned Css through YouTube and free<br></br> resources, developing strong styling<br></br>  skills to create responsive<br></br> web applications.</p>
         </div>
         <div className='card'>
            <img className='js' src='https://static.vecteezy.com/system/resources/previews/027/127/463/non_2x/javascript-logo-javascript-icon-transparent-free-png.png' alt=''></img>
            <p>I learned JavaScript on YouTube<br></br> and through free resources,<br></br> developing strong skills to <br></br>contribute to innovative web development projects</p>
         </div>
         <div className='card'>
            <img className='rjs' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1aQm7GVMjWsqc5KNs_uy5k4LXLVKNeNunDA&s' alt=''></img>
            <p>I learned React.js through YouTube and free resources, developing robust<br></br> front-end skills to enhance and contribute<br></br>to innovative projects.</p>
         </div>
            <div className='card'>
                    <img className='njs' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROqZ5pSJHu7yXCl5n1O2HS5oCdoerZtHly8g&s' alt=''></img>
                    <p>I acquired Node.js skills through<br></br> YouTube and free resources<br></br>, equipping me to contribute effectively<br></br> to modern web development projects.</p>
            </div>
            <div className='card'>
                    <img className='ejs' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI3nGP9w-Ol7H0GYUnDUdCwqnoLwRzoe_cmA&s' alt=''></img>
                    <p> mastered Express.js through YouTube <br></br>and free resources, equipping me to develop robust<br></br> back-end solutions for web applications.</p>
            </div>
        <div className='card'>
                <img className='mdb' src='https://cdn.iconscout.com/icon/free/png-256/free-mongodb-226029.png' alt=''></img>
                <p>I learned MongoDB <br></br>through YouTube and<br></br> free resources, gaining proficiency <br></br>in database management<br></br> for scalable web applications. </p>
        </div>
     </div>  
     
         
         
         
    </div>
  )
}

export default Skill